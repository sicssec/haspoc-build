#!/bin/bash

#
# This file is executed inside the newly created environment to configure it to our liking...
#

sudo apt update

# this is not really needed, but just in case the user has some really crazy setup
sudo apt install -y sudo ufw
sudo ufw allow 22/tcp
sudo ufw logging off
sudo ufw enable

sudo apt install -y git tig tmux mg wget gcc gcc-aarch64-linux-gnu device-tree-compiler bc build-essential cscope

# X via SSH, only for the FVP simulation stuff (FVP uses telnet)
sudo apt install -y --no-install-recommends xauth xterm telnet openssh-server


# install FVP if available
if [ -f /vagrant/data/FM000-KT-00035-r9p1-25rel00.tgz ] ; then
    tar xf /vagrant/data/FM000-KT-00035-r9p1-25rel00.tgz
    sudo mv Foundation_Platformpkg /opt

    cat << EOF >> ~/.bashrc
export PATH=\$PATH:/opt/Foundation_Platformpkg/models/Linux64_GCC-4.1
export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/opt/Foundation_Platformpkg/models/Linux64_GCC-4.1
EOF
fi

git clone https://bitbucket.org/sicssec/haspoc-hypervisor.git ~/haspoc-hypervisor


# run any additional provisioning scripts
[ -f /vagrant/data/provision-private.sh ] && bash /vagrant/data/provision-private.sh

