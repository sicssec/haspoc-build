#!/bin/bash

set -e -x

# get container user write access to shared dirs
chmod a+w shared

lxc launch images:ubuntu/18.04 haspoc
lxc config device add haspoc shareddir disk source=`pwd`/ path=/vagrant # use same name as vagrant

sleep 10 # give it time to start and get the network up

# create user
lxc exec haspoc -- useradd -m -s /bin/bash -G sudo  user
lxc exec haspoc -- passwd -d user

# run provision script as normal user
lxc exec haspoc -- su -l user -c "bash /vagrant/data/provision.sh"


