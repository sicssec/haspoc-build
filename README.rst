
HASPOC build
============

This project contains the build environment for the HASPOC hypervisor. 

The advantage of using this is that you don't need to install a large number of tools and
repositories on your own machine.
Also, it ensures that everyone use the same base configuration.

The builder uses Ubuntu 18.04. You can create it either as a *virtual machine* 
(using Vagrant and Oracle VirtualBox) or a more lightweight *container* (using Canonical LXC and LXD).


As virtual machine
------------------

You need to install vagrant and virtualbox. On Ubuntu this means::

    sudo apt-get install -y vagrant virtualbox virtualbox-ext-pack
    sudo usermod -G vboxusers -a $USER

On OSX and Windows, install this: https://www.vagrantup.com/downloads.html


After the initial setup, start the build machine::

    vagrant up

If successful the VM will start and run the background.
Enter the virtual machine like so::

    vagrant ssh -- -c none -Y

You can put the VM into sleep and wake it up later::

    vagrant suspend
    # VM is now turned off
    vagrant resume

To completely remove the machine::

    vagrant destroy
    rm -rf .vagrant

Note that this removes all your VM files except anything in the shared folder.
The HASPOC build scripts attempt to cache large downloads locally, however these will be removed when you delete the VM.


As container
------------

On most Ubuntu and Debian systems LXC is installed by default, but you may still need to initialize it::

	sudo apt install -y lxc lxd
	sudo lxd init
	newgrp lxd


After the initial setup, create the build container::

    ./container.sh

If successful the container will start and run the background.
Enter the container like so::

	lxc exec haspoc -t -- su -l user


And to remove the container::

    lxc delete -f haspoc


Using the builder
-----------------

First enter the build machine (see above). Then prepare and compile the code::

    cd haspoc-hypervisor
    make setup
    make    

To transfer data to/from the build::

    # from host machine to build
    cp stuff shared/

    # from builder to host machine
	# NOTE: for this to work with containers, we must set shared/ to world writeable
    cp stuff /vagrant/shared/



Simulation
==========

The default build target is the FVP simulator, which is not included here as
it is proprietary ARM software.

However, if you already own a copy place the file FM000-KT-00035-r9p1-25rel00.tgz
in the data folder before you create the builder. This will allow you to run the
"make run" command.
